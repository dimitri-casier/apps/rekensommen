const App = {
    data() {
      return {
        leftMinNumber: 0,
        maxNumber: 20,
        amount: 40,
        exercises: [],
        operators: ["+","-"],
        total: 0
      }
    },
    methods: {
        start(){
            this.total = 0;            
            this.exercises = [{
                left: 9, operator: "-", right: 4, solution: 5, guess: "", score: "",
            },{
                left: 9, operator: "-", right: 3, solution: 6, guess: "", score: "",
            },{
                left: 9, operator: "-", right: 5, solution: 4, guess: "", score: "",
            },{
                left: 9, operator: "-", right: 6, solution: 3, guess: "", score: "",
            },{
                left: 9, operator: "-", right: 2, solution: 7, guess: "", score: "",
            },{
                left: 8, operator: "-", right: 5, solution: 3, guess: "", score: "",
            },{
                left: 8, operator: "-", right: 6, solution: 2, guess: "", score: "",
            },{
                left: 8, operator: "-", right: 3, solution: 5, guess: "", score: "",
            },{
                left: 8, operator: "-", right: 2, solution: 6, guess: "", score: "",
            },{
                left: 7, operator: "-", right: 3, solution: 4, guess: "", score: "",
            },{
                left: 7, operator: "-", right: 2, solution: 5, guess: "", score: "",
            },{
                left: 7, operator: "-", right: 4, solution: 3, guess: "", score: "",
            }];
            
           
            while(this.exercises.length < this.amount) {
                const entry = this.getExercise();
                this.exercises.push(entry);
            }

            
            this.exercises = this.exercises.sort(() => Math.random() - 0.5)
            
        },
        getExercise(){
            let left = this.getNumber(this.leftMinNumber, this.maxNumber);
            let operator = this.getOperator();
            let right = operator == "+" ? this.getNumber(0, this.maxNumber - left) : this.getNumber(0, this.maxNumber);
            let solution = 0;

            if(operator == "-"){
                left = (left < right) ? right : left;
                solution = left - right;
            }else if(operator == "+"){
                solution = left + right;
            }


            return {
                left, operator, right, solution, guess: "", score: ""
            }
        },
        getOperator(){
            const index = Math.floor(Math.random() * this.operators.length);

            return this.operators[index];         
        },
        getNumber(min, max){
            let number =  Math.floor(Math.random() * (max +1));
            while(number < min)
                number =  Math.floor(Math.random() * (max +1));
            
                return number;
        },
        ready(){
            this.total = 0;
            this.exercises.forEach(element => {
                if(parseInt(element.solution) === parseInt(element.guess)){
                    element.score = 1;
                    this.total++;
                }else
                    element.score = 0;
            });
        }

    }
  }
  
  Vue.createApp(App).mount('#app')
